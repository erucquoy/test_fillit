/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit_read.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erucquoy <erucquoy@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/28 15:56:29 by erucquoy          #+#    #+#             */
/*   Updated: 2017/05/04 14:52:11 by erucquoy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"
#include <stdio.h>

char	*t_read(char *file)
{
	int		fd;
	char	*buff;

	// Ouverture du fichier en lecture only
	// S'il ne s'ouvre pas, error
	if ((fd = open(file, O_RDONLY)) < 0)
		ft_error();
	// on alloc un buffer de taille BUFF_SIZE et on l'initialise à 0
	buff = ft_strnew(BUFF_SIZE);
	ft_bzero(buff, BUFF_SIZE); // inutile vu que strnew le fait deja
	// On lit le fichier et on met le contenu dans le buffer.
	// On lit le fichier sur une taille de BUFF_SIZE
	if (read(fd, buff, BUFF_SIZE) < 0)
		ft_error();
	// Si la taille de ce que l'on reçoit est inférieur à 20 error (car moins d'un tetriminos)
	// OU Si le dernier caractère n'est pas 0
	if (ft_strlen(buff) < 20 || buff[BUFF_SIZE - 1] != '\0')
		ft_error();
	// Si le dernier char est un new line et l'avant dernier pas un point ou un hash -> error
	if (buff[ft_strlen(buff) - 1] == '\n' && (buff[ft_strlen(buff) - 2] != '.'
				&& buff[ft_strlen(buff) - 2] != '#'))
		ft_error();
	close(fd);
	// On retourne le buffer
	return (buff);
}
