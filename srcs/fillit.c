/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erucquoy <erucquoy@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/28 15:56:29 by erucquoy          #+#    #+#             */
/*   Updated: 2017/05/04 14:52:41 by erucquoy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"
#include <stdio.h>

void	ft_error(void)
{
	write(2, "error\n", 6);
	exit(EXIT_FAILURE);
}
/*

La fonction fillit va dans un premier temp lire le file avec t_read qu'il stocke dans file_content
t_minos fait une malloc (*27) calqué sur la taille d'un minos

i est initialisé a 20 (taille dun minos) et tant que ce dernier est different de 0
et si le modulo de 21 (minos + \0) est egale a 0
Alors on fait une duplication t_minos 



*/

void	fillit(char *file)
{
	char	*file_content;
	char	**t_minos;
	int		i;
	int		j;

	i = 20;
	j = 0;
	file_content = t_read(file);
	t_minos = (char **)malloc(sizeof(char*) * 27);
	while (file_content[++i] != '\0')
	{
		if (i % 21 == 0)
		{
			t_minos[j] = ft_strdup(t_mino_verif(ft_strsub(
							file_content, i - 21, 19), j));
			j++;
		}
	}
	if (file_content[i] == '\0')
		t_minos[j] = ft_strdup(t_mino_verif(ft_strsub(
						file_content, i - 20, 19), j));
	t_minos[j + 1] = NULL;
	free(file_content);
	t_algo(t_minos, j);
	ft_free_tbl_s(t_minos);
}
/*

le main appelle fillit en lui envoyant en parametre le premier argument uniquement s'il yen a un.
Sinon il affiche "error" grace a ft_error

*/
int		main(int ac, char **av)
{
	if (ac == 2)
		fillit(av[1]);
	else
		ft_error();
	return (0);
}
