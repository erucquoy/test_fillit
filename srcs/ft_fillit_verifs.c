/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit_verifs.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erucquoy <erucquoy@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/28 15:56:29 by erucquoy          #+#    #+#             */
/*   Updated: 2017/05/04 14:51:56 by erucquoy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fillit.h"
#include <stdio.h>

static int		ft_mino_valid(char *t_mino, char mino_nb, int i)
{
	int		token;

	// Token = le nombre de # dans un pattern de tetriminos.
	// Il en faut donc 4
	token = 0;
	if (t_mino[i] == '#' && i >= 0 && i <= 20 && token <= 4)
	{
		token++;
		t_mino[i] = mino_nb + 'a';
		// On remplace le # par la lettre de l'alphabet correspondant
		// (Si c'est le premier tetri, c'est remplacer par a, le deuxieme b, ...)
		// Ensuite on call 4 récursive qui vérifie 4 position différente dans la grille
		// Le char d'apres, d'avant, celui d'en dessous et d'au dessus
		token += ft_mino_valid(t_mino, mino_nb, i + 1);
		token += ft_mino_valid(t_mino, mino_nb, i - 1);
		token += ft_mino_valid(t_mino, mino_nb, i + 5);
		token += ft_mino_valid(t_mino, mino_nb, i - 5);
	}
	return (token);
}

static int		mino_grid_verif(char *t_mino, int mino_nb)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 0;
	while (t_mino[i] != '\0')
	{
		if (t_mino[i] == '.')
			k++;
		if (t_mino[i] == '#')
			// J doit valoir 4. Si ce n'est pas le cas -> error (voir ligne 53)
			j = ft_mino_valid(t_mino, mino_nb, i);
		i++;
	}
	if (i != 19 || j != 4 || k != 12)
		ft_error();
	return (1);
}
#include <stdio.h>

char			*t_mino_verif(char *t_mino, int mino_nb)
{
	printf("|%s|\n", t_mino);
	// Si le tetriminos est verif, alors on le return. Si pas, on continue vers error.
	if (mino_grid_verif(t_mino, mino_nb) == 1)
		return (t_mino);
	ft_error();
	return (NULL);
}
