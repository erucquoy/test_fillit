NAME	= fillit

SRC		= srcs/fillit.c \
		  srcs/ft_fillit_algo.c \
		  srcs/ft_fillit_grids.c \
		  srcs/ft_fillit_read.c \
		  srcs/ft_fillit_verifs.c

OBJ		= $(patsubst srcs/%.c,./%.o,$(SRC))

INCLUDE	= includes

.SILENT:

$(NAME): $(OBJ)
	gcc -Wall -Werror -Wextra $(OBJ) -L libft/ -lft -o $(NAME) -I $(INCLUDE)
	printf '\033[4m'
	printf '\033[32m[ ✔ ] %s\n\033[0m' "fillit is done !"
./%.o: srcs/%.c
	gcc -Wall -Wextra -Werror -c $< -o $@
	printf '\033[0m[ ✔ ] %s\n\033[0m' "$<"

clean:
	/bin/rm -rf *.o
	/bin/rm -rf objs/*.o
	/bin/rm -rf libft/*.o
	printf '\033[31m[ ✔ ] %s\n\033[0m' "Clean"

fclean: clean
	/bin/rm -f $(NAME)
	/bin/rm -f libft/libft.a
	printf '\033[31m[ ✔ ] %s\n\033[0m' "Fclean"

re: fclean all

all: $(NAME)

.PHONY: clean fclean re all